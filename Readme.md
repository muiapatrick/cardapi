This Projects uses java 11 and Mongo database.
The project has swagger documentation integrated within and accessible with url (http://localhost:5556/api/v1/swagger-ui.html)
Instructions how to get the project running
1. Create mongo database DB NAME: carddb
2. add Collections account and card to the mongo db instance
3. set the variable in your computer as
   carddb_mongo_db=carddb #DB NAME
   mongo_host=localhost #DB URL
   mongo_port=27017 #DB PORT
   mongo_user=mongo_user #DB USER
   mongo_password=mongo_password #DB PASSWORD 
4. now ready to execute the project
5. To run project from the code without compiling, open terminal and execute command: mvn spring-boot:run 
6. To Compile execute command: mvn clean package
7. To run compiled jar file execute command: java -jar target/card-0.0.1-SNAPSHOT.jar

https://howtodoinjava.com/spring-boot2/testing/spring-boot-2-junit-5/
https://stackabuse.com/how-to-test-a-spring-boot-application/