package com.kcbcard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter @Setter
public class CardUpdateModel {
    @JsonProperty("card_alias")
    @NotBlank(message = "Card Alias Required")
    private String cardAlias;

    public CardUpdateModel() {
    }
}
