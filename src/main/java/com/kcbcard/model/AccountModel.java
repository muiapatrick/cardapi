package com.kcbcard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter @Setter
public class AccountModel {
    @JsonProperty("iban")
    @NotBlank(message = "IBAN code required")
    private String iban;

    @JsonProperty("bic_swift")
    @NotBlank(message = "SWIFT code required")
    private String bicSwift;

    @JsonProperty("client_id")
    @NotBlank(message = "Client Id required")
    private String clientId;

    public AccountModel() {
    }
}
