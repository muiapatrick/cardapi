package com.kcbcard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter @Setter
public class CardModel {
    @JsonProperty("card_alias")
    @NotBlank(message = "Card Alias Required")
    private String cardAlias;

    @JsonProperty("card_type")
    @NotBlank(message = "Card Type Required")
    private String cardType;

    @JsonProperty("account_id")
    @NotBlank(message = "Account Id Required")
    private String accountId;

    public CardModel() {
    }
}
