package com.kcbcard.collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.annotations.QueryEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.Date;

@QueryEntity
@Document(collection = "account")
@Getter @Setter
public class Account {
    @Id
    private String id;

    @JsonProperty("created_at")
    @Field("created_at")
    private Date createdAt = new Date();

    @JsonProperty("iban")
    @Field("iban")
    private String iban;

    @JsonProperty("bic_swift")
    @Field("bic_swift")
    private String bicSwift;

    @JsonProperty("client_id")
    @Field("client_id")
    private String clientId;

    public Account() {
    }

    public Account(String iban, String bicSwift, String clientId) {
        this.iban = iban;
        this.bicSwift = bicSwift;
        this.clientId = clientId;
    }
}
