package com.kcbcard.collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.annotations.QueryEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.Date;

@QueryEntity
@Document(collection = "card")
@Getter @Setter
public class Card {
    @Id
    private String id;

    @JsonProperty("created_at")
    @Field("created_at")
    private Date createdAt = new Date();

    @JsonProperty("card_alias")
    @Field("card_alias")
    private String cardAlias;

    @JsonProperty("card_type")
    @Field("card_type")
    private String cardType;

    @DBRef
    @JsonProperty("account")
    private Account account;

//    @JsonProperty("account_id")
//    @Field("account_id")
//    private String accountId;

    public Card() {
    }

    public Card(String cardAlias, String cardType, Account account) {
        this.cardAlias = cardAlias;
        this.cardType = cardType;
        this.account = account;
    }
}
