package com.kcbcard.global;

import com.kcbcard.service.MongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** This class helps autowire services in the project once in the whole project, reducing the number of autowires statemente in many classes**/
@Component
public class GlobalService {
    public static MongoService mongoService;

    @Autowired
    public void setMongoService(MongoService mongoService) {
        GlobalService.mongoService = mongoService;
    }
}
