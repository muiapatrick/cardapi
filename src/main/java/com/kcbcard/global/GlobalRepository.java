package com.kcbcard.global;

import com.kcbcard.repository.AccountReportRepository;
import com.kcbcard.repository.CardReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** This class helps autowire repositories once in the whole project, reducing the number of autowires statemente in many classes**/
@Component
public class GlobalRepository {
    public static AccountReportRepository accountReportRepository;
    public static CardReportRepository cardReportRepository;

    @Autowired
    public void setAccountReportRepository(AccountReportRepository accountReportRepository) {
        GlobalRepository.accountReportRepository = accountReportRepository;
    }

    @Autowired
    public void setCardReportRepository(CardReportRepository cardReportRepository) {
        GlobalRepository.cardReportRepository = cardReportRepository;
    }
}
