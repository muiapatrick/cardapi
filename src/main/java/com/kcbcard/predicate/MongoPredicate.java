package com.kcbcard.predicate;

import com.querydsl.core.types.dsl.*;

public class MongoPredicate {

    private SearchCriteria param;
    private PathBuilder<?> entityPath;

    public MongoPredicate(PathBuilder<?> entityPath, SearchCriteria param) {
        this.entityPath = entityPath;
        this.param = param;
    }

    public BooleanExpression getPredicate(){
        if (param.getValue() instanceof String) {
            StringPath path = entityPath.getString(param.getKey());
            if (param.getOperation().equalsIgnoreCase(":")){
                return path.eq(param.getValue().toString());
            }
        }
        else if (param.getValue() instanceof Boolean) {
            BooleanPath path = entityPath.getBoolean(param.getKey());
            boolean value = (boolean) param.getValue();
            return path.eq(value);
        }

        return null;
    }

}
