package com.kcbcard.predicate;


import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class SearchCriteria {
    private String key;
    private String operation;
    private Object value;

    public SearchCriteria(String key, String operation, Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }
    
    public SearchCriteria(String key, String operation) {
        this.key = key;
        this.operation = operation;
    }
}
