package com.kcbcard.util;


public enum ApiCode {
	/**FAILED = 1000 **/
	FAILED (1000),
	/** SUCCESS = 1010 **/
	SUCCESS (1010);

	private final int code;

	private ApiCode(int code) {
		this.code = code;
	}
	public int getCode() {
		return code;
	}
}
