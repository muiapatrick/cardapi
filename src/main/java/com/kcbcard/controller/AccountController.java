package com.kcbcard.controller;

import com.querydsl.core.types.dsl.PathBuilder;
import com.kcbcard.collection.Account;
import com.kcbcard.model.AccountModel;
import com.kcbcard.predicate.MongoPredicateBuilder;
import com.kcbcard.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.kcbcard.global.GlobalService.mongoService;
import static com.kcbcard.util.DataOps.filterRequestParams;

@RestController
@RequestMapping("/account")
@Api(tags = {"Account Endpoints" }, value = "Config", description = "All endpoints for managing accounts", produces = "application/json")
public class AccountController {

    /** CREATE NEW ACCOUNT **/
    @ApiResponses(value={@ApiResponse(code=400, message="Bad Request"), @ApiResponse(code=500, message="Internal Server Error")})
    @ApiOperation(value = "Create Account", httpMethod = "POST", notes = "Creates Account")
    @PostMapping("")
    public ResponseEntity<?> createAccount(@Valid @RequestBody AccountModel model) {
        try {
            //check if account exists with same details
            List<Account> accountList = mongoService.getAccounts(new MongoPredicateBuilder(new PathBuilder<>(Account.class, "account"))
                            .with("clientId", ":", model.getClientId())
                            .with("iban", ":", model.getIban())
                            .with("bicSwift", ":", model.getBicSwift()).build(),
                    PageRequest.of(0, 1, Sort.Direction.ASC, "id")).getContent();

            if (!accountList.isEmpty()) {
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Account with same details exists", null);
                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }

            //create new account now
            Account account = mongoService.saveAccount(new Account(model.getIban(), model.getBicSwift(), model.getClientId()));

            JsonResponse response = JsonSetSuccessResponse.setResponse(ApiCode.SUCCESS.getCode(), "Account Created successfully", null, account);
            return new ResponseEntity(response, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Failed while creating account", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /** UPDATE ACCOUNT **/
    @ApiResponses(value={@ApiResponse(code=400, message="Bad Request"), @ApiResponse(code=500, message="Internal Server Error")})
    @ApiOperation(value = "Update Account", httpMethod = "PUT", notes = "Updates Account")
    @PutMapping("/{id}")
    public ResponseEntity<?> updateAccount(@PathVariable String id, @Valid @RequestBody AccountModel model) {
        try {
            //check if account exists by id
            List<Account> accountList = mongoService.getAccounts(new MongoPredicateBuilder(new PathBuilder<>(Account.class, "account"))
                            .with("id", ":", id).build(),
                    PageRequest.of(0, 1, Sort.Direction.ASC, "id")).getContent();

            if (accountList.isEmpty()) {
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Account you are trying to update does not exists", null);
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }

            //update account now
            Account account = accountList.get(0);
            account.setBicSwift(model.getBicSwift());
            account.setIban(model.getIban());
            mongoService.saveAccount(account);

            JsonResponse response = JsonSetSuccessResponse.setResponse(ApiCode.SUCCESS.getCode(), "Account Updated successfully", null, account);
            return new ResponseEntity(response, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Failed while updating account", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /** DELETE ACCOUNT **/
    @ApiResponses(value={@ApiResponse(code=400, message="Bad Request"), @ApiResponse(code=500, message="Internal Server Error")})
    @ApiOperation(value = "Delete Account", httpMethod = "DELETE", notes = "Deletes Account")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable String id) {
        try {
            //check if account exists by id
            List<Account> accountList = mongoService.getAccounts(new MongoPredicateBuilder(new PathBuilder<>(Account.class, "account"))
                            .with("id", ":", id).build(),
                    PageRequest.of(0, 1, Sort.Direction.ASC, "id")).getContent();

            if (accountList.isEmpty()) {
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Account you are trying to delete exists", null);
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }

            //Delete
            mongoService.deleteAccount(accountList.get(0));

            JsonResponse response = JsonSetSuccessResponse.setResponse(ApiCode.SUCCESS.getCode(), "Account Deleted successfully", null, null);
            return new ResponseEntity(response, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Failed while deleting account", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /** GET LIST OF ACCOUNTS  **/
    @ApiOperation(value = "Returns List of accounts", httpMethod = "GET", notes = "Returns a list of accounts", response = Account.class, responseContainer = "List")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 500, message = "Internal Server Error")})
    @GetMapping(value = "", produces = {"application/json"})
    public ResponseEntity<?> getAccounts(HttpServletRequest request,
                                        @RequestParam(value = "id", required = false) String id,
                                        @RequestParam(value = "iban", required = false) String iban,
                                        @RequestParam(value = "bic_swift", required = false) String bicSwift,
                                         @RequestParam(value = "client_id", required = false) String clientId,
                                        @RequestParam(value = "page", required = false) Integer page,
                                        @RequestParam(value = "page_size", required = false) Integer pageSize) {
        try {
            List<String> unknownParams = filterRequestParams(request, Arrays.asList("id", "iban", "bic_swift", "client_id", "page", "page_size"));
            if (!unknownParams.isEmpty()) {
                // get all errors
                String apiDesc = unknownParams.stream().map(x -> "'" + x.toUpperCase() + "'").collect(Collectors.joining(", ")) + " : Not valid Parameters";
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), apiDesc, null);
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

            int pageNo = (page != null && Integer.valueOf(page) > 0) ? Integer.valueOf(page) - 1 : 0;
            int pageRequestSize = (pageSize != null && Integer.valueOf(pageSize) > 0) ? Integer.valueOf(pageSize) : Integer.MAX_VALUE;

            Page<Account> accounts = mongoService.getAccounts(new MongoPredicateBuilder(new PathBuilder<>(Account.class, "account"))
                            .with("id", ":", id)
                            .with("clientId", ":", clientId)
                            .with("iban", ":", iban)
                            .with("bicSwift", ":", bicSwift).build(),
                    PageRequest.of(pageNo, pageRequestSize, Sort.Direction.ASC, "id"));

            PagenatedJsonResponse response = JsonSetSuccessResponse.setPagenatedResponse(ApiCode.SUCCESS.getCode(), accounts.isEmpty() ? "No accounts Found" : "List of accounts Found", null, accounts);
            return new ResponseEntity(response, accounts.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Error fetching accounts", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
