package com.kcbcard.controller;

import com.querydsl.core.types.dsl.PathBuilder;
import com.kcbcard.collection.Account;
import com.kcbcard.collection.Card;
import com.kcbcard.model.CardModel;
import com.kcbcard.model.CardUpdateModel;
import com.kcbcard.predicate.MongoPredicateBuilder;
import com.kcbcard.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.kcbcard.global.GlobalService.mongoService;
import static com.kcbcard.util.DataOps.filterRequestParams;

@RestController
@RequestMapping("/card")
@Api(tags = {"Card Endpoints" }, value = "Config", description = "All endpoints for managing cards", produces = "application/json")
public class CardController {


    /** CREATE NEW CARD **/
    @ApiResponses(value={@ApiResponse(code=400, message="Bad Request"), @ApiResponse(code=500, message="Internal Server Error")})
    @ApiOperation(value = "Create Card", httpMethod = "POST", notes = "Creates card")
    @PostMapping("")
    public ResponseEntity<?> createCard(@Valid @RequestBody CardModel model) {
        try {
            //check if the account id exists
            //check if account exists by id
            List<Account> accountList = mongoService.getAccounts(new MongoPredicateBuilder(new PathBuilder<>(Account.class, "account"))
                            .with("id", ":", model.getAccountId()).build(),
                    PageRequest.of(0, 1, Sort.Direction.ASC, "id")).getContent();

            if (accountList.isEmpty()) {
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Account you are trying to create card for does not exist", null);
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

            //create new card now
            Card card = mongoService.saveCard(new Card(model.getCardAlias(), model.getCardType(), accountList.get(0)));

            JsonResponse response = JsonSetSuccessResponse.setResponse(ApiCode.SUCCESS.getCode(), "Card Created successfully", null, card);
            return new ResponseEntity(response, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Failed while creating card", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /** UPDATE CARD **/
    @ApiResponses(value={@ApiResponse(code=400, message="Bad Request"), @ApiResponse(code=500, message="Internal Server Error")})
    @ApiOperation(value = "Update Card", httpMethod = "PUT", notes = "Updates Cards")
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCard(@PathVariable String id, @Valid @RequestBody CardUpdateModel model) {
        try {
            //check if card exists by id
            List<Card> cards = mongoService.getCards(new MongoPredicateBuilder(new PathBuilder<>(Card.class, "card"))
                            .with("id", ":", id).build(),
                    PageRequest.of(0, 1, Sort.Direction.ASC, "id")).getContent();

            if (cards.isEmpty()) {
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Card you are trying to update does not exists", null);
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }

            //update card now
            Card card = cards.get(0);
            card.setCardAlias(model.getCardAlias());
            mongoService.saveCard(card);

            JsonResponse response = JsonSetSuccessResponse.setResponse(ApiCode.SUCCESS.getCode(), "Card Updated successfully", null, card);
            return new ResponseEntity(response, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Failed while updating card", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /** DELETE CARD **/
    @ApiResponses(value={@ApiResponse(code=400, message="Bad Request"), @ApiResponse(code=500, message="Internal Server Error")})
    @ApiOperation(value = "Delete Card", httpMethod = "DELETE", notes = "Deletes Card")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCard(@PathVariable String id) {
        try {
            //check if card exists by id
            List<Card> cards = mongoService.getCards(new MongoPredicateBuilder(new PathBuilder<>(Card.class, "card"))
                            .with("id", ":", id).build(),
                    PageRequest.of(0, 1, Sort.Direction.ASC, "id")).getContent();

            if (cards.isEmpty()) {
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Card you are trying to delete exists", null);
                return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }

            //Delete
            mongoService.deleteCard(cards.get(0));

            JsonResponse response = JsonSetSuccessResponse.setResponse(ApiCode.SUCCESS.getCode(), "Card Deleted successfully", null, null);
            return new ResponseEntity(response, HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Failed while deleting card", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /** GET LIST OF CARDS  **/
    @ApiOperation(value = "Returns List of cards", httpMethod = "GET", notes = "Returns a list of cards", response = Card.class, responseContainer = "List")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 500, message = "Internal Server Error")})
    @GetMapping(value = "", produces = {"application/json"})
    public ResponseEntity<?> getCards(HttpServletRequest request,
                                         @RequestParam(value = "id", required = false) String id,
                                         @RequestParam(value = "account_id", required = false) String accountId,
                                         @RequestParam(value = "card_type", required = false) String cardType,
                                         @RequestParam(value = "page", required = false) Integer page,
                                         @RequestParam(value = "page_size", required = false) Integer pageSize) {
        try {
            List<String> unknownParams = filterRequestParams(request, Arrays.asList("id", "account_id", "card_type", "page", "page_size"));
            if (!unknownParams.isEmpty()) {
                // get all errors
                String apiDesc = unknownParams.stream().map(x -> "'" + x.toUpperCase() + "'").collect(Collectors.joining(", ")) + " : Not valid Parameters";
                JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), apiDesc, null);
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

            int pageNo = (page != null && Integer.valueOf(page) > 0) ? Integer.valueOf(page) - 1 : 0;
            int pageRequestSize = (pageSize != null && Integer.valueOf(pageSize) > 0) ? Integer.valueOf(pageSize) : Integer.MAX_VALUE;

            Page<Card> cards = mongoService.getCards(new MongoPredicateBuilder(new PathBuilder<>(Card.class, "card"))
                    .with("id", ":", id)
                    .with("cardType", ":", cardType)
                    .with("account.id", ":", accountId).build(), PageRequest.of(pageNo, pageRequestSize, Sort.Direction.ASC, "id"));

            PagenatedJsonResponse response = JsonSetSuccessResponse.setPagenatedResponse(ApiCode.SUCCESS.getCode(), cards.isEmpty() ? "No cards Found" : "List of cards Found", null, cards);
            return new ResponseEntity(response, cards.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        catch (Exception e) {
            e.printStackTrace();
            JsonResponse response = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Error fetching cards", null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
