package com.kcbcard.service;

import com.querydsl.core.types.Predicate;
import com.kcbcard.collection.Account;
import com.kcbcard.collection.Card;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/** Service to handle data saving and reading from database **/
public interface MongoService {
    Account saveAccount(Account account); //save account
    void deleteAccount(Account account); //deletes account entry from db completely
    Page<Account> getAccounts(Predicate predicate, PageRequest pageRequest); //retrieve accounts

    Card saveCard(Card card); //save card
    void deleteCard(Card card); //deletes card entry from db completely
    Page<Card> getCards(Predicate predicate, PageRequest pageRequest); //retrieve cards
}
