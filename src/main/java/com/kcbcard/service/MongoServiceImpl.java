package com.kcbcard.service;

import com.querydsl.core.types.Predicate;
import com.kcbcard.collection.Account;
import com.kcbcard.collection.Card;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.kcbcard.global.GlobalRepository.accountReportRepository;
import static com.kcbcard.global.GlobalRepository.cardReportRepository;

@Transactional("mongoTransactionManager")
@Service("mongoService")
public class MongoServiceImpl implements MongoService {

    @Override
    public Account saveAccount(Account account) {
        return accountReportRepository.save(account);
    }

    @Override
    public void deleteAccount(Account account) {
        accountReportRepository.delete(account);
    }

    @Override
    public Page<Account> getAccounts(Predicate predicate, PageRequest pageRequest) {
        return predicate == null ? accountReportRepository.findAll(pageRequest) :  accountReportRepository.findAll(predicate, pageRequest);
    }

    @Override
    public Card saveCard(Card card) {
        return cardReportRepository.save(card);
    }

    @Override
    public void deleteCard(Card card) {
        cardReportRepository.delete(card);
    }

    @Override
    public Page<Card> getCards(Predicate predicate, PageRequest pageRequest) {
        return predicate == null ? cardReportRepository.findAll(pageRequest) :  cardReportRepository.findAll(predicate, pageRequest);
    }
}
