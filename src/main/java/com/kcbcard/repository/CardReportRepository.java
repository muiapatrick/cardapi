package com.kcbcard.repository;
import com.kcbcard.collection.Card;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CardReportRepository extends MongoRepository<Card, String>, QuerydslPredicateExecutor<Card> {
}
