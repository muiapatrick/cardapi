package com.kcbcard.repository;
import com.kcbcard.collection.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountReportRepository extends MongoRepository<Account, String>, QuerydslPredicateExecutor<Account> {
}
