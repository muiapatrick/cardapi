package com.kcbcard.errorhandler;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class ValidationError {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<FieldErrorMv> errors = new ArrayList<>();

    public ValidationError() {
    }

    public void addValidationError(FieldErrorMv error){
        errors.add(error);
    }
}
