package com.kcbcard.errorhandler;

import com.kcbcard.util.ApiCode;
import com.kcbcard.util.JsonErrorResponse;
import com.kcbcard.util.JsonSetErrorResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LogManager.getLogger(RestResponseEntityExceptionHandler.class);


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ValidationError error = ValidationErrorBuilder.fromBindingErrors(ex.getBindingResult());
        JsonErrorResponse jsonResponse = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Request Failed", null, error.getErrors());
        return new ResponseEntity<>(jsonResponse, headers, status);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        JsonErrorResponse jsonResponse = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Missing Parameter", null, ex.getParameterName() +" is required!!");
        return new ResponseEntity<>(jsonResponse, headers, status);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleMissingServletRequestPart(ex, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String msg = "Invalid Field Value "+ex.getValue();
        JsonErrorResponse jsonResponse = JsonSetErrorResponse.setResponse(ApiCode.FAILED.getCode(), "Invalid Type", null, msg);
        return new ResponseEntity<>(jsonResponse, headers, status);
    }
}


