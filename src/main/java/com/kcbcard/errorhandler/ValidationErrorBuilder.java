package com.kcbcard.errorhandler;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class ValidationErrorBuilder {
    public static ValidationError fromBindingErrors(Errors errors){
        ValidationError error = new ValidationError();
        for(ObjectError objectError : errors.getAllErrors()){
            if (objectError instanceof FieldError) {
                FieldError fieldError = (FieldError) objectError;
                error.addValidationError(new FieldErrorMv(null, fieldError.getDefaultMessage(), fieldError.getRejectedValue()));
            }
        }
        return error;
    }
}
