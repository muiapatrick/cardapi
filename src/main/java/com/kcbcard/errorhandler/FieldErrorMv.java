package com.kcbcard.errorhandler;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FieldErrorMv {

    @JsonProperty("field")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fieldName;

    @JsonProperty("error")
    private String error;

    @JsonProperty("rejected_value")
    private Object rejectedValue;

    public FieldErrorMv() {
    }

    public FieldErrorMv(String fieldName, String error, Object rejectedValue) {
        this.fieldName = fieldName;
        this.error = error;
        this.rejectedValue = rejectedValue;
    }
}
