package com.kcbcard;

import com.kcbcard.controller.AccountController;
import com.kcbcard.controller.CardController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//@ExtendWith(SpringExtension.class)
@SpringBootTest
class CardApplicationTests {

	@Autowired
	private CardController cardController;

	@Autowired
	private AccountController accountController;

	@Test
	void contextLoads() {
		//test if application loads successfully
		Assertions.assertThat(cardController).isNotNull();
		Assertions.assertThat(accountController).isNotNull();
	}

}
